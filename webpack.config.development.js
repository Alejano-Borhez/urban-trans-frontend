const path = require('path');
const webpack = require('webpack');

const baseConfig = require('./webpack.config.base');

const config = Object.create(baseConfig);

config.entry = {
  client: ['babel-polyfill', 'react-hot-loader/patch', 'webpack-dev-server/client?http://localhost:9000', './src/index'],
  vendors: './src/vendors',
};

config.plugins.push(
  new webpack.NamedModulesPlugin(),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.DefinePlugin({
    PRODUCTION: JSON.stringify(false),
  }),
);

config.devServer = {
  contentBase: path.join(__dirname, 'public'),
  historyApiFallback: true,
  compress: true,
  port: 9000,
  open: true,
  hot: true,
};

config.devtool = 'source-map';

config.watch = true;

module.exports = [config];
