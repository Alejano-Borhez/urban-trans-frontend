# urban-trans-frontend
Urban Trans application is designed as a tracking & billing system on means of public transportation: buses, trolleybuses, trams, metro, funicular, intercity, route taxis. The main idea of a project and main way to implement this system - is a use of IoT solution: eCard + onboard reader.

## How to run it
    npm install
    npm start // devServer
    npm run prod <dirName> // production build with optional prop dirName
    npm run build // development build

## Contributors
* [Andrey Shubich](https://github.com/shubich)
* [Eugene Ivankin](https://github.com/EugeneIvankin)
* Egor Burkovski
