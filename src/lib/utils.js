export const jsonToQueryString = json => `?${
  Object.keys(json).map(key => `${encodeURIComponent(key)}=${
    encodeURIComponent(json[key])}`).join('&')}`;

export const something = '1010';
