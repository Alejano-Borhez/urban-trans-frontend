import { jsonToQueryString } from './utils';

let host;

if (!PRODUCTION) {
  console.log('Debug info');
  host = 'http://epbybrew0006t17:8080/';
}

if (PRODUCTION) {
  console.log('Production log');
  host = '/';
}

const paths = {
  rides: 'bil/view/user/',
};

export const fetchJson = url => fetch(url).then(res => res.json());

export const requests = {
  rides: (payload) => {
    const queryString = jsonToQueryString(payload.params);
    const url = `${host}${paths.rides}${payload.id}${queryString}`;
    return fetchJson(url);
  },
};

// Поездки пользователя
// http://epbybrew0006t17:8080/bil/view/user/2a9d1b40-08ed-4aaa-bb68-4e9f85ad114e?dateFrom=2017-12-13&dateTo=2017-12-15&count=50&offset=0
