export const createRoute = (map, startPoint, finishPoint) => {
  if (startPoint !== '' && finishPoint !== '') {
    const multiRouteModel = new ymaps.multiRouter.MultiRouteModel([
      startPoint,
      finishPoint,
    ], {
      wayPointDraggable: true,
      boundsAutoApply: true,
    });
    const multiRoute = new ymaps.multiRouter.MultiRoute(multiRouteModel, {
      wayPointDraggable: true,
      boundsAutoApply: true,
    });
    map.geoObjects.add(multiRoute);
  }
};

export const lol = 'kek';
