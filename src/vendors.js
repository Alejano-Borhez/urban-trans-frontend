import 'react';
import 'react-dom';
import 'react-hot-loader';
import 'react-router-dom';
import 'react-redux';
import 'antd';
import 'antd/lib/locale-provider/ru_RU';
import 'moment';
