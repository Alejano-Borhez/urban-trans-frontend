import { put, takeEvery, call } from 'redux-saga/effects';
import * as actions from '../actions/ridesActions';
import * as types from '../constants/Rides';
import * as Api from '../lib/api';

export function* getRides(action) {
  yield put(actions.getRidesRequest());
  try {
    const json = yield call(Api.requests.rides, action.payload);
    yield put(actions.getRidesSuccess(json));
  } catch (e) {
    yield put(actions.getRidesFailure(e.message));
  }
}

export default function* watchRidesAsync() {
  yield takeEvery(types.GET_RIDES_ASYNC, getRides);
}
