import { all } from 'redux-saga/effects';

// import sagas
import watchRidesAsync from './rides';

export default function* rootSaga() {
  yield all([
    // put sagas here
    watchRidesAsync(),
  ]);
}
