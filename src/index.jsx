import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import { LocaleProvider } from 'antd';
import ruRU from 'antd/lib/locale-provider/ru_RU';
import 'antd/dist/antd.less';
import configureStore from './store/configureStore';
import App from './components/App';
import './styles/ant-custom.less';
import './styles/main.less';

const store = configureStore();

const render = () => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <Router>
          <LocaleProvider locale={ruRU}>
            <App />
          </LocaleProvider>
        </Router>
      </Provider>
    </AppContainer>,
    document.getElementById('root'),
  );
};

if (module.hot) {
  module.hot.accept('./components/App', render);
}

render();
