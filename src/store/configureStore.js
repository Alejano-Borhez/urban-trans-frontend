import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';

// import reducers
// ...
import user from '../reducers/user';
import rides from '../reducers/rides';
import rideDetails from '../reducers/rideDetails';
import map from '../reducers/map';
import route from '../reducers/route';

import rootSaga from '../sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();

const urbanTransApp = combineReducers({
  // put reducers here
  user,
  rides,
  rideDetails,
  map,
  route,
});

export default (/* initialState */) => {
  const store = createStore(
    urbanTransApp,
    // initialState,
    applyMiddleware(sagaMiddleware),
  );

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);
  sagaMiddleware.run(rootSaga);

  return store;
};
