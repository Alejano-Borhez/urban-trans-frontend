import * as types from '../constants/Rides';

export const getRidesAsync = payload => ({
  type: types.GET_RIDES_ASYNC,
  payload,
});

export const getRidesRequest = id => ({
  type: types.GET_RIDES_REQUEST,
  id,
});

export const getRidesSuccess = rides => ({
  type: types.GET_RIDES_SUCCESS,
  rides,
});

export const getRidesFailure = error => ({
  type: types.GET_RIDES_FAILURE,
  error,
});

export const resetRides = () => ({
  type: types.RESET_RIDES,
});

export const setDates = payload => ({
  type: types.SET_DATES,
  payload,
});
