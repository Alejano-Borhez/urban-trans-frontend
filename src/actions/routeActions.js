import * as types from '../constants/Route';

export const setRoute = route => ({
  type: types.SET_ROUTE,
  route,
});
