import * as types from '../constants/RideDetails';

export const setRideDetails = details => ({
  type: types.SET_DETAILS,
  details,
});

export const resetRideDetails = () => ({
  type: types.RESET_DETAILS,
});
