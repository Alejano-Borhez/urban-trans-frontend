import * as types from '../constants/Map';

const createMapSuccess = map => ({
  type: types.CREATE_MAP,
  map,
});

export const createMap = () => {
  const myMap = new ymaps.Map('map', {
    center: [55.750625, 37.626],
    zoom: 7,
    controls: [],
  }, {
    buttonMaxWidth: 300,
  });
  return createMapSuccess(myMap);
};

