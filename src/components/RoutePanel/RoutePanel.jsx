import React from 'react';
import { Input } from 'antd';
import { Button } from 'antd/lib/radio';
import CircleIcon from '../CircleIcon';

import './RoutePanel.less';

const RoutePanel = () => (
  <div className="RouterPanel">
    <Input
      prefix={<CircleIcon className="StartPoint" />}
      className="inputPoint"
      placeholder="startPoint"
    />
    <Input
      prefix={<CircleIcon className="FinishPoint" />}
      className="inputPoint"
      placeholder="finishPoint"
    />
    <Button>Create</Button>
  </div>
);

export default RoutePanel;
