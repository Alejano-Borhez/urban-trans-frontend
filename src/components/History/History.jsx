import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { DatePicker } from 'antd';
import Nav from '../Nav';
import RidesContainer from '../../containers/RidesContainer';
import './History.less';

export default class History extends React.Component {
  componentWillMount() {
    this.parse = 'YYYY-MM-DD';
    this.dates = {
      from: moment().subtract(1, 'months').format(this.parse),
      to: moment().format(this.parse),
    };
    this.props.setDates(this.dates);
  }

  updateDates = (key, val) => {
    this.dates[key] = val.format(this.parse);
    this.props.setDates(this.dates);
  }

  handleChangeFrom = (date) => {
    this.updateDates('from', date);
  }

  handleChangeTo = (date) => {
    this.updateDates('to', date);
  }

  render() {
    return (
      <div className="History">
        <Nav title="История" prevPage="/profile" />
        <div className="Container">
          <div className="History-Dates">
            <span className="History-DateFrom">С</span>
            <DatePicker
              className="History-Date"
              placeholder="Начальная дата"
              defaultValue={moment().subtract(1, 'months')}
              onChange={this.handleChangeFrom}
            />
            <span className="History-DateTo">По</span>
            <DatePicker
              className="History-Date"
              placeholder="Конечная дата"
              defaultValue={moment(this.dates.to, this.dates.parse)}
              onChange={this.handleChangeTo}
            />
          </div>
          <div className="History-Info">
            <div className="History-RidesCount" title="api?">
            Поездок: 34
            </div>
            <div className="History-MoneySpent" title="api?">
            Потрачено: 30.45 BYN
            </div>
          </div>
        </div>
        <RidesContainer />
      </div>
    );
  }
}

History.propTypes = {
  setDates: PropTypes.func.isRequired,
};
