import React from 'react';
import { Link } from 'react-router-dom';
import Balance from '../Balance';
import Rides from '../../containers/RidesContainer';
import Nav from '../Nav';
import './Profile.less';

const Profile = () => (
  <div className="Profile">
    <Nav title="Пользователь" prevPage="/" />
    <Balance />
    <div className="Container Profile-Container">
      <h2 className="Profile-Title">Последние поездки</h2>
      <Link to="/profile/history" className="Profile-Link">
        подробнее
      </Link>
    </div>
    <Rides />
  </div>
);

export default Profile;
