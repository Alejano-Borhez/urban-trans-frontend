import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Ride from '../../containers/RideContainer';
import RideDetails from '../../containers/RideDetailsContainer';
import './Rides.less';

export default class Rides extends React.Component {
  componentWillMount() {
    this.lastDate = 0;
    this.payload = {
      id: '2a9d1b40-08ed-4aaa-bb68-4e9f85ad114e',
      params: {
        dateFrom: this.props.dates.from,
        dateTo: this.props.dates.to,
        count: 20,
        offset: 0,
      },
    };

    if (!this.props.fetching) {
      this.props.getRidesAsync(this.payload);
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillReceiveProps(nextProps) {
    if (
      (this.props.dates.to !== nextProps.dates.to
      || this.props.dates.from !== nextProps.dates.from)
      && !this.props.fetching
    ) {
      this.payload.params.dateFrom = nextProps.dates.from;
      this.payload.params.dateTo = nextProps.dates.to;

      this.lastDate = 0;
      this.props.resetRides();
      this.props.getRidesAsync(this.payload);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
    this.props.resetRides();
  }

  handleScroll = (/* e */) => {
    if (window.innerHeight + window.scrollY > document.querySelector('.Rides').clientHeight - 100) {
      if (!this.props.fetching && this.props.rides.ridesCount === 20) {
        // console.log('I need to load some more content here…');
        this.payload.params.offset += this.payload.params.count;
        this.props.getRidesAsync(this.payload);
      }
    }
  }

  render() {
    return (
      <div className="Rides">
        <div className="Container Rides-Container">
          <RideDetails />
          {
            this.props.rides.previousRides.length ? (
              this.props.rides.previousRides.map((ride) => {
                let showDate = false;
                if (!moment(this.lastDate).isSame(ride.timeStamp, 'day')) {
                  this.lastDate = ride.timeStamp;
                  showDate = true;
                }
                return <Ride key={ride.id} {...ride} showDate={showDate} />;
              })
            ) : (
              <span>У вас нет поездок за этот период.</span>
            )

          }
        </div>
      </div>

    );
  }
}

Rides.propTypes = {
  fetching: PropTypes.bool.isRequired,
  getRidesAsync: PropTypes.func.isRequired,
  resetRides: PropTypes.func.isRequired,
  rides: PropTypes.shape({
    currentRide: PropTypes.object,
    previousRides: PropTypes.array,
    ridesCount: PropTypes.number,
  }).isRequired,
  dates: PropTypes.shape({
    from: PropTypes.string,
    to: PropTypes.string,
  }).isRequired,
};
