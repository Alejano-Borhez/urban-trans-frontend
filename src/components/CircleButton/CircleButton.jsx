import React from 'react';

import './CircleButton.less';

const CircleButton = props => (
  <button className={`CircleButton ${props.className}`} onClick={props.onClick}>
    {props.children}
  </button>
);

export default CircleButton;
