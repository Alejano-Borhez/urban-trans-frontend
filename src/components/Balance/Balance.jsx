import React from 'react';
import { Button } from 'antd';

import './Balance.less';

const Balance = () => (
  <div className="Balance">
    <div className="Container Balance-Container">
      <h2 className="Balance-title">Ваш баланс</h2>
      <div className="Balance-Info">
        <span className="Balance-Value">16.45 рублей</span>
        <Button size="large">Пополнить</Button>
      </div>
    </div>
  </div>
);

export default Balance;
