import React, { Component } from 'react';
import { Icon } from 'antd';
import { Link } from 'react-router-dom';
import CircleBytton from '../CircleButton';

import './MenuNavigation.less';

export default class MenuNavigation extends Component {
    createRoute = () => {
      console.log('create route');
    };

    openUserProfile = () => {
      console.log('open profile');
    };

    render() {
      return (
        <nav className="MenuNavigation">
          <CircleBytton className="RouteButon" onClick={this.createRoute} />

          <Link to="/profile">
            <CircleBytton onClick={this.openUserProfile}>
              <Icon className="LogoUser" type="user" />
            </CircleBytton>
          </Link>
        </nav>
      );
    }
}
