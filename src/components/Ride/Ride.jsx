import React from 'react';
import PropTypes from 'prop-types';

import { Icon } from 'antd';

import './Ride.less';

export default class Ride extends React.Component {
  handleRideClick = () => {
    this.props.setRideDetails(this.props);

    this.props.setRoute({
      startPoint: [this.props.busStopBegin.gpsLatitude, this.props.busStopBegin.gpsLongitude],
      finishPoint: [this.props.busStopEnd.gpsLatitude, this.props.busStopEnd.gpsLongitude],
    });
  }

  render() {
    const dateDisplay = this.props.showDate ? '' : 'Ride-Date_hidden';

    return (
      <div className="Ride" onClick={this.handleRideClick}>

        <div className={`Ride-Date ${dateDisplay}`}>
          {new Date(this.props.timeStamp).toLocaleString('en-GB', { day: '2-digit', month: '2-digit', year: '2-digit' })}
        </div>

        <div className="Ride-Graph">
          <div className="Ride-Circle Ride-Circle_color_red" />
          <Icon type="arrow-down" className="Ride-Arrow" />
          <div className="Ride-Circle Ride-Circle_color_blue" />
        </div>

        <div className="Ride-Places">
          <div className="Ride-From">
            {this.props.busStopBegin.description}
          </div>
          <div className="Ride-To">
            {this.props.busStopEnd.description}
          </div>
        </div>

        <div className="Ride-Timestamps">
          <div className="Ride-BoardingTime">
            {new Date(this.props.positionBegin.timeStamp).toLocaleString('ru', { hour: '2-digit', minute: '2-digit' })}
          </div>
          <div className="Ride-LandingTime">
            {new Date(this.props.positionEnd.timeStamp).toLocaleString('ru', { hour: '2-digit', minute: '2-digit' })}
          </div>
        </div>

        <div className={`Ride-Transport Ride-Transport_type_${'t' || 'a'}`}>
          <div className="Ride-TransportNumber">
            {this.props.route.description[0]}
          </div>
          <div className="Ride-TransportType">
            {this.props.route.description.split(' ')[1]}
          </div>
        </div>

        <div className="Ride-Payment">
          <div className="Ride-PaymentValue">{this.props.amount}</div>
          <div className="Ride-PaymentCurrency">BYN</div>
        </div>
      </div>
    );
  }
}

Ride.propTypes = {
  setRideDetails: PropTypes.func.isRequired,
  setRoute: PropTypes.func.isRequired,
  busStopBegin: PropTypes.shape({
    gpsLatitude: PropTypes.number.isRequired,
    gpsLongitude: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
  busStopEnd: PropTypes.shape({
    gpsLatitude: PropTypes.number.isRequired,
    gpsLongitude: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
  positionBegin: PropTypes.shape({
    timeStamp: PropTypes.number.isRequired,
  }).isRequired,
  positionEnd: PropTypes.shape({
    timeStamp: PropTypes.number.isRequired,
  }).isRequired,
  timeStamp: PropTypes.string.isRequired,
  route: PropTypes.shape({
    routeId: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
  amount: PropTypes.number.isRequired,
  // timeOnBoard: PropTypes.string.isRequired,
  showDate: PropTypes.bool.isRequired,
};

Ride.defaultProps = {
};
