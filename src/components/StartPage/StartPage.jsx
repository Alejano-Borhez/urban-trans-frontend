import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RoutePanel from '../RoutePanel';
import MapContainer from '../../containers/MapContainer';
import MenuNavigation from '../MenuNavigation';

import './StartPage.less';

export default class StartPage extends Component {
  componentDidMount() {

  }

  componentWillReceiveProps() {

  }


  render() {
    return (
      <div className="StartPage">
        <RoutePanel />
        <MapContainer />
        <MenuNavigation />
      </div>
    );
  }
}
