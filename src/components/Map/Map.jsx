import React, { Component } from 'react';
import CircleButton from '../CircleButton';
import PropTypes from 'prop-types';

import './Map.less';

export default class Map extends Component {
  componentDidMount() {
    ymaps.ready(() => {
      this.props.createMap();
      this.props.createRoute(this.props.map, this.props.startPoint, this.props.finishPoint);
    });
  }

  render() {
    return (
      <div id="map" className={`Map ${this.props.className}`} />
    );
  }
}
