import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'antd';
import Map from '../../containers/MapContainer';

import './RideDetails.less';

export default class RideDetails extends React.Component {
  componentWillMount() {

  }

  handleCancel = () => {
    this.props.resetRideDetails();
  }

  render() {
    if (!this.props.display) return null;

    return (
      <Modal
        className="RideDetails"
        zIndex={9999}
        width={650}
        title="Детали поездки"
        visible
        footer={false}
        onCancel={this.handleCancel}
      >
        <div className="RideDetails-Map">
          <Map />
        </div>

        <div className="RideDetails-Info">

          <div className="RideDetails-RouteDescription">
            {this.props.details.route.description}
          </div>

          <div className="RideDetails-Row">
            <div>Расстояние</div>
            <div>{`${this.props.details.mileage} км`}</div>
          </div>

          <div className="RideDetails-Row">
            <div>Время в пути</div>
            <div>{this.props.details.timeOnBoard}</div>
          </div>

          <div className="RideDetails-Row">
            <div>Сумма</div>
            <div>{`${this.props.details.amount} BYN`}</div>
          </div>

          <div className="RideDetails-Row">
            <div className="RideDetails-PointFrom">
              {this.props.details.busStopBegin.description}
            </div>
            <div className="RideDetails-TimeFrom">
              {new Date(this.props.details.positionBegin.timeStamp).toLocaleString('en-GB')}
            </div>
          </div>

          <div className="RideDetails-Row">
            <div className="RideDetails-PointTo">
              {this.props.details.busStopEnd.description}
            </div>
            <div className="RideDetails-TimeTo">
              {new Date(this.props.details.positionEnd.timeStamp).toLocaleString('en-GB')}
            </div>
          </div>

        </div>
      </Modal>
    );
  }
}

RideDetails.propTypes = {
  resetRideDetails: PropTypes.func.isRequired,
  display: PropTypes.bool.isRequired,
  details: PropTypes.objectOf(PropTypes.any).isRequired,
};

RideDetails.defaultProps = {
};
