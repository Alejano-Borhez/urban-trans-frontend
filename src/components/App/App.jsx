import React from 'react';
import { Route, Switch } from 'react-router-dom';

import StartPage from '../StartPage';
import Profile from '../Profile';
import HistoryContainer from '../../containers/HistoryContainer';

import './App.less';

const App = () => (
  <div className="App">
    <Switch>
      <Route exact path="/profile">
        <Profile />
      </Route>
      <Route path="/profile/history">
        <HistoryContainer />
      </Route>
      <Route path="*">
        <StartPage />
      </Route>
    </Switch>
  </div>
);

export default App;
