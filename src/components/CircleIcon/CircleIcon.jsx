import React from 'react';

import './CircleIcon.less';

const CircleIcon = props => (
  <div className={`CircleIcon ${props.className}`} />
);

export default CircleIcon;
