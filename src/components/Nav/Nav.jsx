import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Icon, Button } from 'antd';

import './Nav.less';

const Nav = props => (
  <nav className="Nav">
    <div className="Container Nav-Container">
      <Link to={props.prevPage}>
        <Button className="Nav-Button"><Icon type="left" /></Button>
      </Link>
      <h1 className="Nav-Title">{props.title}</h1>
    </div>
  </nav>
);


Nav.propTypes = {
  prevPage: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

Nav.defaultProps = {
};


export default Nav;
