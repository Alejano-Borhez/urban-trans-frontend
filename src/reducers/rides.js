import * as types from '../constants/Rides';

// import ridesJSON from '../rides.json';

const initialState = {
  // rides: ridesJSON,
  fetching: false,
  error: '',
  rides: {
    currentRide: null,
    previousRides: [],
  },
  dates: {
    from: '',
    to: '',
  },
};

const rides = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_RIDES_REQUEST:
      return { ...state, fetching: true };
    case types.GET_RIDES_SUCCESS:
      return {
        ...state,
        fetching: false,
        error: '',
        rides: {
          currentRide: action.rides.currentRide,
          previousRides: [...state.rides.previousRides, ...action.rides.previousRides],
          ridesCount: action.rides.ridesCount,
        },
      };
    case types.GET_RIDES_FAILURE:
      return { ...state, fetching: false, error: action.error };
    case types.SET_DATES:
      return { ...state, dates: { ...action.payload } };
    case types.RESET_RIDES:
      return { ...initialState, dates: state.dates };
    default:
      return state;
  }
};

export default rides;
