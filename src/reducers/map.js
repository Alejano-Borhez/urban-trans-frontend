import * as types from '../constants/Map';


const initialState = {
  map: {},
};


const map = (state = initialState, action) => {
  switch (action.type) {
    case types.CREATE_MAP:
      return { ...initialState, map: action.map };
    default:
      return state;
  }
};

export default map;
