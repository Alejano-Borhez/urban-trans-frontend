import * as types from '../constants/Route';

const initialState = {
  startPoint: [52.110122736588465, 23.750789165496826],
  finishPoint: [52.086511146547785, 23.696898221969604],
};

const route = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ROUTE:
      return { ...action.route };
    default:
      return state;
  }
};

export default route;
