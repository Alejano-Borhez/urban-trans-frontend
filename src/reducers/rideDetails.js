import * as types from '../constants/RideDetails';

const initialState = {
  details: {},
  display: false,
};

const rideDetails = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_DETAILS:
      return { details: action.details, display: true };
    case types.RESET_DETAILS:
      return { details: {}, display: false };
    default:
      return state;
  }
};

export default rideDetails;
