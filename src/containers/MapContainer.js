import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createRoute } from '../lib/map';
// import { getRoute } from '../actions/getRoute';
import { createMap } from '../actions/createMap';
import Map from '../components/Map';


const mapStateToProps = state => ({
  ...state.route,
  ...state.map,
  createRoute,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  // getRoute,
  createMap,
}, dispatch);

const MapContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Map);

export default MapContainer;
