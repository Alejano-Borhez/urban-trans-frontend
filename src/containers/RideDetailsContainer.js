import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RideDetails from '../components/RideDetails';
import { resetRideDetails } from '../actions/rideDetailsActions';


const mapStateToProps = state => ({
  ...state.rideDetails,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  resetRideDetails,
}, dispatch);

const RideDetailsContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RideDetails);

export default RideDetailsContainer;
