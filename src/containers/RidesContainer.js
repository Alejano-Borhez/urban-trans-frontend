import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Rides from '../components/Rides';
import { getRidesAsync, resetRides } from '../actions/ridesActions';

const mapStateToProps = state => ({
  ...state.rides,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getRidesAsync,
  resetRides,
}, dispatch);

const RideHistoryContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Rides);

export default RideHistoryContainer;
