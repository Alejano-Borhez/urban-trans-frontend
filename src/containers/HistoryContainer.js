import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import History from '../components/History';
import { setDates } from '../actions/ridesActions';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  dates: state.rides.dates,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setDates,
}, dispatch);

const HistoryContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(History);

export default HistoryContainer;
