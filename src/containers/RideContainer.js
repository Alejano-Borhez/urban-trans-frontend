import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Ride from '../components/Ride';
import { setRideDetails, resetRideDetails } from '../actions/rideDetailsActions';
import { setRoute } from '../actions/routeActions';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setRideDetails,
  resetRideDetails,
  setRoute,
}, dispatch);

const RideHistoryContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Ride);

export default RideHistoryContainer;
