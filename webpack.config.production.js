const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const baseConfig = require('./webpack.config.base');

const config = Object.create(baseConfig);

const customPath = process.argv[5] ? (
  `/${process.argv[5]}/`
) : ('/');

config.entry = {
  client: ['babel-polyfill', './src/index'],
  vendors: './src/vendors',
};

config.output = {
  filename: '[name].bundle.js',
  path: path.join(__dirname, `public${customPath}`),
  publicPath: customPath,
};

config.plugins.push(
  new UglifyJSPlugin(),
  new webpack.DefinePlugin({
    PRODUCTION: JSON.stringify(true),
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'production'),
    },
  }),
);

module.exports = [config];
